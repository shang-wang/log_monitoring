This is a simulation of HTTP log monitoring and alerting system.

# installation:

The only dependency for this program to run is `python-dateutil`, used for calculating time difference. To install the dependency, create a virtualenv first then do:

    pip install -r requirements.txt

# How to run the program

## Run the simulation

There are 2 program involves in this simulation, log generator and the console app. Log generator would simulates a real web server that writes logs to a log file, console app would be the actual tool to monitor the log file. To start the log program, do:

    python log_generator.py >> access.log

Here the log generator would append log outputs to the file `access.log`.

Then in another window, we start the monitoring program:

    python console_app.py -l access.log

Here the flag `-l` is used to specify which log file does the monitoring app monitors. There are other 2 flags to configure our monitoring app so we could achieve some flexibility here. Here's the help output of the app:

```
usage: console_app.py [-h] [-l LOG_FILE] [-t ALERT_WINDOW]
                      [-c ALERT_COUNT_THRESHOLD]

optional arguments:
  -h, --help            show this help message and exit
  -l LOG_FILE           Log file to parse
  -t ALERT_WINDOW       Alert time window in minutes, default to 2 minutes
  -c ALERT_COUNT_THRESHOLD
                        Average hit threshold per minute to alert on, default to 150
```

## Run the tests

The tests for the app is in file `tests.py`. To run the tests, simply do:

    python tests.py

# Output explanation

Here's a sample output:

```
Most hit url: /pricing, count: 12
Average hits per minute: 36.00
Host that generates the most requests: 133.142.72.254, count: 9
User that generates the most requests: admin, count: 10
Response code stats:
code: 200, count: 31
code: 404, count: 5
Buffer size is: 36
Buffered time window: 0:00:09

Most hit url: /pricing, count: 23
Average hits per minute: 70.00
Host that generates the most requests: 133.142.72.254, count: 11
User that generates the most requests: dev, count: 19
Response code stats:
code: 200, count: 58
code: 404, count: 11
code: 403, count: 1
Buffer size is: 70
Buffered time window: 0:00:19

...

High traffic generated an alert - hits = 101.00 hits/minute, triggered at 2016-11-09 10:41:33

Most hit url: /about, count: 141
Average hits per minute: 114.00
Host that generates the most requests: 71.137.152.129, count: 52
User that generates the most requests: admin, count: 127
Response code stats:
code: 200, count: 391
code: 404, count: 43
code: 403, count: 21
Buffer size is: 114
Buffered time window: 0:01:00

...

High traffic alert recovered - hits = 96.00 hits/minute, recovered time: 2016-11-09 10:43:50

...
```

Every 10 seconds the app would print a set of information to the stdout:

* Most hit url: The url that got the most hits
* Average hits per minute: The average traffic per minute
* Host that generates the most requests: The host that hits the web server the most
* User that generates the most requests: The request user that hits the web server the most
* Response code stats: The statistics of the http response by response code
* Buffer size: The amount of traffic that current buffer is holding(will address the usage of buffer later)
* Buffered time window: The duration of the data that current buffer is holding

# How it works

## Reading the input

The monitoring app starts by getting the log file and move the cursor to the end of the file. This is to make sure that our monitoring data is only valid in real time. Whenever a new line is written into the log file, the app would read it and start to analyze the content. A regular expression is used to extract useful information for that piece of log.

## Countings, periodic output and alertings

We maintain several internal hash tables as counter tracker for different statistics, like url hit, host count, user count and response code count. The other critical data structure is a queue acts as a buffer for alerting and averaging purpose. It would always hold the timestamps for the logs in the past X minutes, X being the amount of time we take the average on to do the alerting(per requirement it's 2 minutes).

When a new log record comes in, we compare the timestamp with the oldest record in the queue. If the difference is less than 2 minutes, we simply enqueue this timestamp. If the difference is greater than 2 minutes, we keep doing the dequeue for the oldest timestamps until the buffer has only timestamps that's within the last 2 minutes. By doing this we use `O(n)` space to store the data, with n being the number of logs received in the last 2 minutes, and only `O(1)` time complexity to update the buffer.

The 10s periodic task is done in a separate thread. It simply checks our counters and buffer size then print the results to the screen. It's daemonized because it's only useful when the main program is up.

The alerting is checked immediately after the buffer is updated by simply comparing the buffer size and our threshold. If the buffer holds more timestamps for the last 2 minutes than the threshold, the alert is triggered.

## Log generator

It's not part of the solution, but it's worth mentioning here the tricks in it. It contains several examples of every part of the log like urls, hosts, users, response code etc, then randomize them and combine them as output. it also has 2 levels of output frequency. It starts with the faster mode, doing something between 2 ~ 10 logs per second. After 2 minutes the generator would switch to slow mode, at the speed of 1 ~ 2 logs per second. It flips back to fast mode 2 minutes later and it cycles on. It would make our simulation easier because the alerts would appear soon after the generator switches gears.

# Things to improve

## Use a time series database

This would address several issues that this simple simulation has. First, having a database allows us to keep the history as we go along. This would let us do the post-mortem analysis on historical data, study the traffic pattern, define thresholds, etc. We could also have multiple alertings and more complex logic defined that uses different ranges of data, or simply graph the traffic load.

## Alerting improvement

As of this implementation, it immediately alerts on traffic that goes over/under the threshold, but only once for each event. This might not be desired for the situation when the traffic is going over the threshold. Most likely when that happens, developers need to be alerted persistently so they are aware of the fact that the problem doesn't go away, and they will most likely to notice the alerts eventually if they missed initial alerts. This is not currently implemented but would be helpful.

## Separate control interface

The app currently keeps running by itself, but it doesn't have any interface for engineers to control it. Ideally, our app needs a separate interface that people could give instruction to. The operations includes start/pause/stop the app, acknowledge an alert, change the threshold at run time, etc.