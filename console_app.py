#!/usr/bin/env python

from collections import defaultdict, Counter, deque
from dateutil import relativedelta

import datetime
import threading
import argparse
import signal
import time
import re
import os

MINUTE = 60

LOG_REGEX = '^(?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) - (?P<user>\w+) ' \
            '\[(?P<timestamp>\d{2}/\w{3}/\d{4}:(([01]\d|2[0-3]):([0-5]\d)|24:00):\w{2}) -\w{4}] ' \
            '"GET (?P<url>\/.+\/? )HTTP 1.0" (?P<status_code>\d{3}) ' \
            '(?P<response_size>\d+)$'


class LogProcessor(object):
    """
    Log parser that takes a log file and parse new lines
    """
    log_regex = re.compile(LOG_REGEX)

    def __init__(self, log_file, alert_count_threshold, alert_window=2):
        """
        Constructor of log processor
        log_file is the file to parse
        alert_count_threshold is average traffic threshold that we need to alert on
        alert_window is how far back do we look at the average in minutes,
        default to 2 minutes
        """
        self.log_file = log_file
        self.alert_window = alert_window * MINUTE
        self.alert_count_threshold = alert_count_threshold

        self.url_counter = defaultdict(int)
        self.source_counter = defaultdict(int)
        self.user_counter = defaultdict(int)
        self.status_code_counter = defaultdict(int)
        # this is a queue that only holds requests from last x minutes
        self.request_timestamps = deque()

        # flag to terminate the processor loop
        self.terminate = False
        # binary flag to indicate alert or recover state
        self.is_alerting = False

        signal.signal(signal.SIGINT, self.stop)

    def stop(self, signum, frame):
        """
        Signal trigger to shutdown the processor
        """
        self.terminate = True

    def parse_timestamp(self, timestamp):
        """
        parse a timestamp string and return a datetime object
        """
        time_obj = datetime.datetime.strptime(timestamp, '%d/%b/%Y:%H:%M:%S')
        return time_obj

    def read_file(self):
        """
        Log file read function. Always go to the end of the file and wait
        for new lines to appear
        """
        with open(self.log_file, 'r') as f:
            # go to the end of the log file
            f.seek(0, os.SEEK_END)
            while True:
                # the stop signal is set, quit
                if self.terminate:
                    break

                line = f.readline()
                if not line:
                    time.sleep(0.1)
                    continue
                yield line

    def display_10s_message(self, stats):
        """
        Functon only responsible for printing 10s data
        """
        if not stats:
            print 'There is no log to parse at this time.'
            return

        print 'Most hit url: %s, count: %s' % (stats['top_url'][0],
                                               stats['top_url'][1])

        print 'Average hits per minute: %.2f' % stats['avg_request_count']

        print 'Host that generates the most requests: %s, count: %s' % \
              (stats['top_source'][0], stats['top_source'][1])

        print 'User that generates the most requests: %s, count: %s' % \
              (stats['top_user'][0], stats['top_user'][1])

        print 'Response code stats:'
        for code, count in stats['status_code_stats']:
            print 'code: %s, count: %s' % (code, count)

        print 'Buffer size is: %s' % stats['queue_size']
        print 'Buffered time window: %s' % stats['queued_time']
        print ''

    def process_10s_stats(self):
        """
        Take stats snapshot and return that for print
        """
        if not self.url_counter:
            return None
        # use Counter.most_common() to quickly determine top values
        top_url = Counter(self.url_counter).most_common(1)[0]
        top_source = Counter(self.source_counter).most_common(1)[0]
        top_user = Counter(self.user_counter).most_common(1)[0]
        status_code_stats = Counter(self.status_code_counter).most_common(5)

        alert_time_threshold = self.alert_window / 60
        avg_request_count = len(self.request_timestamps) / float(alert_time_threshold)

        queued_time = self.request_timestamps[-1] - self.request_timestamps[0]
        return {'top_url': top_url,
                'top_source': top_source,
                'top_user': top_user,
                'status_code_stats': status_code_stats,
                'queue_size': len(self.request_timestamps),
                'avg_request_count': avg_request_count,
                'queued_time': queued_time}

    def top_hit_stats(self):
        """
        Cron like function that executed every 10s and display top status

        We report:
        Top hit url
        Host that sent the most requests
        Average hits per minute
        User that sent the most requests
        Response code stats for all requests(like 200, 403, 404)
        Buffer size for timestamps
        Total buffered time
        """
        while True:
            time.sleep(10)
            result = self.process_10s_stats()
            self.display_10s_message(result)

    def _extract_section(self, url):
        """
        Take a url and extract the section of it
        """
        # discard all empty split results
        paths = [i for i in url.split('/') if i]
        return '/%s' % paths[0]

    def update_counters(self, parse_result, alert_window):
        """
        Update internal counters based on parse result of a line
        alert_window is the time window for alerting, used for
        updating timestamp queue
        """
        # get url section and update counters
        self.url_counter[parse_result['url_section']] += 1
        self.source_counter[parse_result['source']] += 1
        self.user_counter[parse_result['user']] += 1
        self.status_code_counter[parse_result['status_code']] += 1

        timestamp = parse_result['timestamp']
        # add timestamp to the queue
        self.request_timestamps.append(timestamp)
        # past threshold is the timestamp exactly x minutes ago
        past_threshold = timestamp - alert_window
        # remove timestamps earlier than x minutes from queue
        while self.request_timestamps and self.request_timestamps[0] < past_threshold:
            self.request_timestamps.popleft()

    def alert_on_threshold(self, timestamp):
        """
        This function is responsible for alerting on request count over
        threshold for the last x minutes
        """
        # get average logs per minute
        alert_time_threshold = self.alert_window / 60
        avg_request_count = len(self.request_timestamps) / float(alert_time_threshold)

        if avg_request_count > self.alert_count_threshold and not self.is_alerting:
            print ''
            print 'High traffic generated an alert - hits = %.2f hits/minute, ' \
                  'triggered at %s' % (avg_request_count, str(timestamp))
            print ''
            self.is_alerting = True
        elif avg_request_count <= self.alert_count_threshold and self.is_alerting:
            print ''
            print 'High traffic alert recovered - hits = %.2f hits/minute, ' \
                  'recovered time: %s' % (avg_request_count, str(timestamp))
            print ''
            self.is_alerting = False

    def parse_line(self, line):
        """
        Take one line of log and try to extract information from it
        """
        match = self.log_regex.match(line)
        if match:
            url_section = self._extract_section(match.groupdict()['url'])
            source = match.groupdict()['source']
            user = match.groupdict()['user']
            status_code = match.groupdict()['status_code']
            timestamp = self.parse_timestamp(match.groupdict()['timestamp'])
            return {'url_section': url_section,
                    'source': source,
                    'user': user,
                    'status_code': status_code,
                    'timestamp': timestamp}
        else:
            return None

    def start(self):
        """
        Main loop to process log file
        """
        # take the generator returned from read_file function
        line_generator = self.read_file()

        # spin up a separate thread to do the every 10s stats report
        thread = threading.Thread(target=self.top_hit_stats)
        # daemonize the thread so we don't worry about clean up
        thread.daemon = True
        thread.start()

        # convert alert time from number of seconds to timedelta
        alert_window = relativedelta.relativedelta(seconds=self.alert_window)

        while True:
            try:
                # read a line
                line = next(line_generator).strip()
                parse_result = self.parse_line(line)
                if parse_result:
                    self.update_counters(parse_result, alert_window)
                    self.alert_on_threshold(parse_result['timestamp'])
                if self.terminate:
                    print 'console app is shutting down...'
                    time.sleep(0.1)
                    break
            except StopIteration:
                self.terminate = True
                print 'console app is shutting down...'
                break


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l',
                        action='store',
                        dest='log_file',
                        help='Log file to parse')
    parser.add_argument('-t',
                        action='store',
                        default=2,
                        dest='alert_window',
                        type=int,
                        help='Alert time window in minutes, default to 2 minutes')
    parser.add_argument('-c',
                        action='store',
                        default=150,
                        dest='alert_count_threshold',
                        type=int,
                        help='Average hit threshold per minute to alert on, default to 150')

    args = parser.parse_args()

    if not vars(args)['log_file']:
        parser.print_help()
        parser.exit(1)

    processor = LogProcessor(args.log_file, args.alert_count_threshold, args.alert_window)
    processor.start()

if __name__ == '__main__':
    main()
