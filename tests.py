from console_app import LogProcessor
from dateutil    import relativedelta

import datetime
import unittest


class LogProcessorTest(unittest.TestCase):
    def setUp(self):
        # alert_count
        self.alert_count = 5
        self.log_processor = LogProcessor('dummy.log', self.alert_count)
        # create a list of logs that span across 2 minutes(default value for log processor)
        self.logs = ['200.13.5.97 - admin [08/Nov/2016:13:00:00 -0500] "GET /careers/ HTTP 1.0" 200 3804',
                     '200.13.5.97 - devops [08/Nov/2016:13:00:30 -0500] "GET /foo/baz/ HTTP 1.0" 404 4054',
                     '200.13.5.97 - dev [08/Nov/2016:13:01:00 -0500] "GET /resources/ HTTP 1.0" 200 3219',
                     '53.9.83.216 - admin [08/Nov/2016:13:01:10 -0500] "GET /about/press/ HTTP 1.0" 200 1878',
                     '71.137.152.129 - qa [08/Nov/2016:13:01:30 -0500] "GET /test/test/ HTTP 1.0" 404 1110',
                     '167.218.110.163 - dev [08/Nov/2016:13:01:50 -0500] "GET /about/team HTTP 1.0" 200 3601']

        overflow_logs = ['200.13.5.97 - admin [08/Nov/2016:13:00:00 -0500] "GET /careers/ HTTP 1.0" 200 3804',
                         '200.13.5.97 - devops [08/Nov/2016:13:00:10 -0500] "GET /foo/baz/ HTTP 1.0" 404 4054',
                         '200.13.5.97 - dev [08/Nov/2016:13:00:20 -0500] "GET /resources/ HTTP 1.0" 200 3219',
                         '53.9.83.216 - admin [08/Nov/2016:13:00:30 -0500] "GET /about/press/ HTTP 1.0" 200 1878',
                         '5.91.32.16 - qa [08/Nov/2016:13:00:40 -0500] "GET /about/haha/ HTTP 1.0" 404 1878',
                         '71.137.152.129 - qa [08/Nov/2016:13:01:00 -0500] "GET /test/test/ HTTP 1.0" 404 1110',
                         '200.13.5.97 - admin [08/Nov/2016:13:01:10 -0500] "GET /careers/ HTTP 1.0" 200 3804',
                         '200.13.5.97 - devops [08/Nov/2016:13:01:20 -0500] "GET /foo/baz/ HTTP 1.0" 404 4054',
                         '200.13.5.97 - dev [08/Nov/2016:13:01:30 -0500] "GET /resources/ HTTP 1.0" 200 3219',
                         '53.9.83.216 - admin [08/Nov/2016:13:01:40 -0500] "GET /about/press/ HTTP 1.0" 200 1878',
                         '5.91.32.16 - qa [08/Nov/2016:13:01:40 -0500] "GET /about/haha/ HTTP 1.0" 404 1878',
                         '71.137.152.129 - qa [08/Nov/2016:13:01:50 -0500] "GET /test/test/ HTTP 1.0" 404 1110',
                         '167.218.110.163 - dev [08/Nov/2016:13:02:10 -0500] "GET /about/team HTTP 1.0" 200 3601']
        self.overflow_logs = overflow_logs

        self.recover_log = '200.13.5.97 - admin [08/Nov/2016:13:03:50 -0500] "GET /careers/ HTTP 1.0" 200 3804'

    def test_parse_line(self):
        """
        Test if the parser works for log parsing
        """
        log1 = '11.2.29.7 - devops [08/Nov/2016:11:41:14 -0500] "GET /pricing/pro HTTP 1.0" 200 3633'
        log2 = '200.13.5.97 - admin [08/Nov/2016:11:42:29 -0500] "GET /resources/test/ HTTP 1.0" 200 3341'
        log3 = '200.13.5.97 - admin [08/Nov/2016:11:42:29 -0500] "GET haha HTTP 1.0" 200 3341'

        log1_expect = {'url_section': '/pricing',
                       'source': '11.2.29.7',
                       'user': 'devops',
                       'status_code': '200',
                       'timestamp': datetime.datetime(2016, 11, 8, 11, 41, 14)}

        log2_expect = {'url_section': '/resources',
                       'source': '200.13.5.97',
                       'user': 'admin',
                       'status_code': '200',
                       'timestamp': datetime.datetime(2016, 11, 8, 11, 42, 29)}
        self.assertDictEqual(self.log_processor.parse_line(log1), log1_expect)
        self.assertDictEqual(self.log_processor.parse_line(log2), log2_expect)
        self.assertEqual(self.log_processor.parse_line(log3), None)

    def test_stats_counters(self):
        """
        This would actually test our function that gets called every 10s to see
        if we could correctly update the counter
        """
        alert_window = relativedelta.relativedelta(seconds=self.log_processor.alert_window)
        # use log processor to go through each log so the internal counters are updated
        for log in self.logs:
            log_result = self.log_processor.parse_line(log)
            self.log_processor.update_counters(log_result, alert_window)

        # test internal counters
        self.assertDictEqual(dict(self.log_processor.url_counter),
                             {'/careers': 1, '/foo': 1, '/resources': 1, '/test': 1, '/about': 2})
        self.assertDictEqual(dict(self.log_processor.source_counter),
                             {'200.13.5.97': 3, '53.9.83.216': 1, '71.137.152.129': 1, '167.218.110.163': 1})
        self.assertDictEqual(dict(self.log_processor.user_counter),
                             {'admin': 2, 'devops': 1, 'qa': 1, 'dev': 2})
        self.assertDictEqual(dict(self.log_processor.status_code_counter),
                             {'200': 4, '404': 2})
        self.assertEqual(len(self.log_processor.request_timestamps), len(self.logs))

    def test_alert_window_threshold(self):
        """
        This is to test if the alert window threshold works.
        """
        alert_window = relativedelta.relativedelta(seconds=self.log_processor.alert_window)
        for log in self.overflow_logs:
            log_result = self.log_processor.parse_line(log)
            self.log_processor.update_counters(log_result, alert_window)

        # check if the internal buffer drops one request because the first record
        # falls out of the 2 minute range
        self.assertEqual(len(self.log_processor.request_timestamps), len(self.overflow_logs) - 1)
        # calls alert_on_threshold() and see if the is_alerting flag is set
        # which equals to say the alert has been printed

        self.log_processor.alert_on_threshold(log_result['timestamp'])
        self.assertEqual(self.log_processor.is_alerting, True)

        # the recover log has a much later timestamp, if processed, the console app
        # would wipe out many records in our buffer so the average for the last
        # 2 minutes drops below threshold
        recover_log_result = self.log_processor.parse_line(self.recover_log)
        self.log_processor.update_counters(recover_log_result, alert_window)
        self.assertLess(len(self.log_processor.request_timestamps), self.alert_count)
        self.log_processor.alert_on_threshold(recover_log_result['timestamp'])
        self.assertEqual(self.log_processor.is_alerting, False)

if __name__ == '__main__':
    unittest.main()
