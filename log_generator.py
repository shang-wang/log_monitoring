#!/usr/bin/env python

import time
import random
import datetime
import sys
import signal


class LogGenerator(object):
    """
    A class to generate log file. Simulating an active web log
    """
    ips = ['116.220.209.78',
           '13.159.150.241',
           '202.114.121.175',
           '145.176.59.37',
           '200.13.5.97',
           '71.137.152.129',
           '53.9.83.216',
           '167.218.110.163',
           '133.142.72.254',
           '37.184.115.15']

    uids = ['dev', 'devops', 'admin', 'qa']

    success_urls = ['/about/contact/',
                    '/about/team/',
                    '/about/press/',
                    '/resources/',
                    '/careers/',
                    '/product/integrations',
                    '/product/management',
                    '/pricing/enterprise/yearly',
                    '/pricing/pro/monthly',
                    '/pricing/free/perm']

    perm_denied_urls = ['/private/asset/',
                        '/employee-income/yearly',
                        '/employee-income/quarterly',
                        '/employee-income/monthly',
                        '/salary/ceo/',
                        '/salary/cto/',
                        '/about/revenue/']

    not_found_urls = ['/abc/foo/',
                      '/foo/bar/',
                      '/foo/baz/',
                      '/test/test/',
                      '/about/hack']
    # flag to quit
    terminate = False

    def stop_generator(self, signum, frame):
        self.terminate = True

    def __init__(self):
        self.set_url_weights()
        signal.signal(signal.SIGINT, self.stop_generator)

    def get_weighted_item(self, items):
        """
        This function would do a random weighted choice
        """
        total = sum(weight for item, weight in items)
        r = random.uniform(0, total)
        upto = 0
        for item, weight in items:
            if upto + weight >= r:
                return item
            upto += weight

    def generate_ip_address(self):
        """
        Generate a random ip address
        """
        return self.ips[random.randint(0, len(self.ips) - 1)]

    def generate_uid(self):
        """
        Randomly pick a user from the list
        """
        return self.uids[random.randint(0, len(self.uids) - 1)]

    def generate_timestamp(self):
        """
        Get current time and return a standard timestamp
        """
        now = datetime.datetime.now()
        time_str = now.strftime('%d/%b/%Y:%H:%M:%S')
        return '[%s %s]' % (time_str, '-0500')

    def set_url_weights(self):
        """
        Give each url a random weight
        """
        self.weighted_200_urls = self.randomize_urls(self.success_urls)
        self.weighted_403_urls = self.randomize_urls(self.perm_denied_urls)
        self.weighted_404_urls = self.randomize_urls(self.not_found_urls)

    def randomize_urls(self, urls):
        """
        Label each url with an associated weight
        """
        nums = range(len(urls), 0, -1)
        random.shuffle(urls)
        return zip(urls, nums)

    def generate_response_size(self):
        """
        Generate random response size
        """
        return random.randint(512, 4096)

    def generate_log_item(self):
        """
        Get one line of log for simulation
        """
        weighted_response = (('200', 100), ('404', 10), ('403', 8))
        result = ''
        result += self.generate_ip_address() + ' - '
        result += self.generate_uid() + ' '
        result += self.generate_timestamp() + ' '

        response = self.get_weighted_item(weighted_response)
        if response == '200':
            url = self.get_weighted_item(self.weighted_200_urls)
        elif response == '403':
            url = self.get_weighted_item(self.weighted_403_urls)
        elif response == '404':
            url = self.get_weighted_item(self.weighted_404_urls)

        result += '"GET ' + url + ' HTTP 1.0" '
        result += '%s %s' % (response, self.generate_response_size())
        return result

    def generate_log(self):
        """
        Driver program to generate log to stdout

        We have a switch that controls the frequency of the log output
        Every 2 minutes the switch would flip and the frequency would
        go from slow to fast or the other way round
        """
        start_time = datetime.datetime.now()
        switch = -1

        while True:
            log = self.generate_log_item()
            print log
            # don't buffer anything
            sys.stdout.flush()
            # if current time is 2 minutes away from start time,
            # flip the switch
            current = datetime.datetime.now()
            if current - start_time >= datetime.timedelta(minutes=2):
                switch *= -1
                start_time = current

            # test the state of switch to delay longer/shorter
            if switch > 0:
                milliseconds = random.randint(500, 1000)
            else:
                milliseconds = random.randint(100, 500)
            time.sleep(milliseconds / 1000.0)
            if self.terminate:
                print 'log generator is shutting down...'
                break


def main():
    gen = LogGenerator()
    gen.generate_log()


if __name__ == '__main__':
    main()
